const express = require('express');

const secure = require('./secure');
const response = require('../../../network/response');
const controller = require('./index');

const router = express.Router();

//ROUTES
router.get('/', list);
router.get('/:id', get);
router.post('/', upsert);
router.put('/', secure('update'), upsert);

//internal funtions

function list(req, res, next){
    controller.list()
        .then((list) => {
            response.succes(req, res, list, 200);
        })
        .catch(next);
}

function get(req, res, next){
    controller.list()
        .then((provider) => {
            response.succes(req, res, provider, 200);
        })
        .catch(next);
}

function upsert(req, res, next){
    controller.upsert(req.body)
        .then((provider) => {
            response.succes(req, res, provider, 201);
        })
        .catch(next);
}

module.exports = router;