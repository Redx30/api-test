const nanoid = require('nanoid');
const auth = require('../auth');

const TABLA = 'provider';

module.exports = function(injectedStore) {
    let store = injectedStore;
    if(!store){
        store = require('../../../store/mysql');
    }

    function list(){
        return store.list(TABLA);
    }

    function get(id){
        return store.get(TABLA, id);
    }

    async function upsert(body){
        const provider = {
            name: body.name,
            username: body.username,
            nit: body.nit,
            address: body.address,
            namecharge:  body.namecharge,
            numberphone: body.numberphone,
            email: body.email,
        }

        if (body.id) {
            provider.id = body.id
        } else {
            provider.id = nanoid();
        }

        if(body.password || body.username){
            await auth.upsert({
                id: provider.id,
                username: provider.username,
                password: body.password,
            });
        }

        return store.upsert(TABLA, provider);
    }

    return{
        list,
        get,
        upsert,
    }
}