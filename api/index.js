const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
// const swaggerUi = require('swagger-ui-express');

const config = require('../config');
const user = require('./components/user/network');
const provider = require('./components/provider/network');
const auth = require('./components/auth/network');
const errors = require('../network/errors');

const app = express();

app.use(cors());
app.use(bodyParser.json());

// const swaggerDoc = require('./swagger.json')

//ROUTER
app.use('/api/user', user);
app.use('/api/provider', provider);
app.use('/api/auth', auth);

// app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDoc));

app.use(errors);

app.listen(config.api.port, () => {
    console.log('API start in the port', config.api.port);
});