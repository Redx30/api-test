const express = require('express');

const response = require('../../../network/response');
const auth = require('./secure');
const controller = require('./index');

const router = express.Router();

//set routes
//no se necesita autenticacion
router.get('/', list);
//se necesita autenticacion
router.get('/', auth('list'), list);
router.get('/:id', auth('get'), get);
router.post('/', auth('add'), upsert);
router.put('/', auth('update', {owner: `provider`}), upsert);

//funtions
function list(req, res, next) {
    controller.list()
    .then(product => {
        response.succes(req, res, product, 200);
    })
    .catch(next);
}

function get(req, res, next){
    controller.get(req.params.id)
        .then(product => {
            response.succes(req, res, product, 200);
        })
        .catch(next);
}

function upsert(req, res, next){
    controller.upsert(req.body, req.user.id)
        .then(product => {
            response.succes(req, res, product, 201);
        })
        .catch(next);
}

module.exports = router;