const nanoid = require('nanoid');
const error = require('../../../utils/error');

const COLLECTION = 'product';

module.exports = function(injectedStore){
    let store = injectedStore;
    if (!store) {
        store = require('../../../store/mysql');
    }

    function list(query){
        return store.list(COLLECTION);
    }

    async function get(id){
        const user = await store.get(COLLECTION, id);

        if (!user) {
            throw error('No existe el producto', 404);
        }

        return user;
    }

    async function upsert(body, user) {
        const product ={
            id: body.id,
            user: user,
            name: body.name,
            ref: body.ref,
            med: body.med,
            quantity: body.quantity,
            weight: body.weight,
            cost: body.cost,
            units: body.units,
        }

        if (!product.id) {
			product.id = nanoid();
		}

        return store.upsert(COLLECTION, product).then(() => product);
    }
    
    return{
        list,
        get,
        upsert,
    }
}