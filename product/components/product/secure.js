const auth = require('../../../auth');
const controller = require('./index');

function checkAuth(action, options) {
    async function middleware(req, res, next) {
        switch(action) {
            case 'add':
            auth.check.logged(req);
            next()
            break;

            case 'update':
                const product = await controller.get(req.body.id);
                auth.check.own(req, product.user);
                next()
                break;

            default:
                next();
        }
    }
    return middleware;
}

module.exports = checkAuth;