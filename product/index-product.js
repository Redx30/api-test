const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const config = require('../config');
const product = require('./components/product/network');
const errors = require('../network/errors');

const app = express();

app.use(cors());
app.use(bodyParser.json());

//router
app.use('/api/product', product);


app.use(errors);

app.listen(config.product.port, ()=> {
    console.log('Service product start in the port', config.product.port);
});